<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    private const FILE_NAME = 'names.json';
    private array $list;


    public function __construct(
    ) {
        if (!file_exists(self::FILE_NAME)) {
            file_put_contents(self::FILE_NAME, json_encode(['names' => []]));
        }
        $this->list = json_decode(file_get_contents(self::FILE_NAME), true) ?? [];
    }

    #[Route(path: '/roulette', name: 'roulette', methods: ['GET'])]
    public function mainPage()
    {
        $names = [];
        return $this->render('roulette.html.twig', ['names' => $names]);
    }

    #[Route(path: '/result', name: 'result', methods: ['POST'])]
    public function result()
    {
        $number = random_int(0, count($this->names())-1);
        return $this->render('result.html.twig', [
            'name' => $this->names()[$number],
            'names'=> $this->names(),
        ]);
    }

    #[Route(path: '/settings', name: 'settings', methods: ['GET'])]
    public function settings()
    {
        return $this->render('settings.html.twig', [
            'names'=> $this->names(),
        ]);
    }

    #[Route(path: '/add_name', name: 'add_name', methods: ['POST'])]
    public function add_name(Request $request)
    {
        $name = $request->get('add_name');
        $this->list['names'][$name] = 1;
        $this->save();
        return $this->redirectToRoute('settings');
    }

    #[Route(path: '/delete_name', name: 'delete_name', methods: ['GET'])]
    public function delete_name(Request $request)
    {
        $name = $request->get('delete_name');
        unset($this->list['names'][$name]);
        $this->save();
        return $this->redirectToRoute('settings');
    }

    private function save()
    {
        file_put_contents(self::FILE_NAME, json_encode($this->list));
    }

    private function names()
    {
        return array_keys($this->list['names']);
    }
}
