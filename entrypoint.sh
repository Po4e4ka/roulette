export $(grep -v '^#' .env | sed -E 's/=[[:space:]]*/=/g' | xargs)
unitd --control unix:/var/run/control.unit.sock
curl -X PUT --data-binary @./.unit.conf.json --unix-socket /var/run/control.unit.sock http://localhost/config/
tail -f /var/log/unit.log