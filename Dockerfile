FROM unit:1.31.0-php8.2

ARG USER
ARG USER_GID
ARG USER_UID

RUN groupmod -g $USER_GID $USER && \
    usermod -d /home/$USER/ -u $USER_UID $USER

RUN apt-get update && apt-get install -y \
    libzip-dev

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN docker-php-ext-install zip

USER $USER
WORKDIR /home/$USER/www
COPY --chown=$USER:$USER ./ /home/$USER/www

RUN composer install --no-scripts

CMD ["/bin/sh", "entrypoint.sh"]
